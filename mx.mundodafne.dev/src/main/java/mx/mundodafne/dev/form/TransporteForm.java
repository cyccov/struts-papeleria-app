package mx.mundodafne.dev.form;

import org.apache.struts.action.ActionForm;

public class TransporteForm extends ActionForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1012814518019225920L;
	private String valorQueViaja;

	public String getValorQueViaja() {
		return valorQueViaja;
	}

	public void setValorQueViaja(String valorQueViaja) {
		this.valorQueViaja = valorQueViaja;
	}
	
	
}
