package mx.mundodafne.dev.form;

import org.apache.struts.action.ActionForm;

public class UsuarioConsultaForm extends ActionForm {
	private long idUsuario;
	private String nombreUsuario;
	public long getIdUsuario() {
		return idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
	
}
