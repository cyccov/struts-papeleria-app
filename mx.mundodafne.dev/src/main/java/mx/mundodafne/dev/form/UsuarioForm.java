package mx.mundodafne.dev.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

import mx.mundodafne.dev.vo.UsuarioVO;

/**
 * Form para dar de alta a un nuevo usuario en BD.
 * */
public class UsuarioForm extends ActionForm{
	
	private long idUsuario;
	private String nombreUsuario;
	private String passwordUsuario;
	private List rolesUsuario;
	private String rolUsuario;
	private List<UsuarioVO> listaUsuarios;
	
	
	
	public List<UsuarioVO> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuarioVO> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public String getRolUsuario() {
		return rolUsuario;
	}

	public void setRolUsuario(String rolUsuario) {
		this.rolUsuario = rolUsuario;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getPasswordUsuario() {
		return passwordUsuario;
	}

	public void setPasswordUsuario(String passwordUsuario) {
		this.passwordUsuario = passwordUsuario;
	}

	public List getRolesUsuario() {
		return rolesUsuario;
	}

	public void setRolesUsuario(List rolesUsuario) {
		this.rolesUsuario = rolesUsuario;
	}
}
