package mx.mundodafne.dev.form;

import java.util.Date;

import org.apache.struts.action.ActionForm;

public class ProveedorForm extends ActionForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7074484793117254435L;
	private long idProveedor;
	private String rfcProveedor;
	private String razonSocial;
	private String direccion;
	private Date fechaAlta;
	private Date fechaBaja;
	private Date ultimaActualizacion;
	public long getIdProveedor() {
		return idProveedor;
	}
	public String getRfcProveedor() {
		return rfcProveedor;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public String getDireccion() {
		return direccion;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public void setIdProveedor(long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public void setRfcProveedor(String rfcProveedor) {
		this.rfcProveedor = rfcProveedor;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
}