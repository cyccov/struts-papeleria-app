package mx.mundodafne.dev.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.validator.ValidatorForm;

import static mx.mundodafne.dev.util.Constantes.ERROR_ROL_USUARIO_DESCRIPCION_ROL_MISSING;

public class RolUsuarioForm extends ValidatorForm{

	private String descripcionRol;
	private int nivelPermiso;
	
	
	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors actionErrors = new ActionErrors();
		if (null == descripcionRol) {
			actionErrors.add("descripcionRol",new ActionMessage(ERROR_ROL_USUARIO_DESCRIPCION_ROL_MISSING));
		}
		return super.validate(mapping, request);
	}
	
	public String getDescripcionRol() {
		return descripcionRol;
	}
	public int getNivelPermiso() {
		return nivelPermiso;
	}
	public void setDescripcionRol(String descripcionRol) {
		this.descripcionRol = descripcionRol;
	}
	public void setNivelPermiso(int nivelPermiso) {
		this.nivelPermiso = nivelPermiso;
	}
	
	
	
	
}
