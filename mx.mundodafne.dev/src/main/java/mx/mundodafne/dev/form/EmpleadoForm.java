package mx.mundodafne.dev.form;

import java.io.Serializable;
import java.util.Date;

import org.apache.struts.action.ActionForm;

import mx.mundodafne.dev.entity.Usuario;

public class EmpleadoForm extends ActionForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8605420329534416079L;
	private String nombreUno;
	private String nombreDos;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String rfc;
	private Date fechaNacimiento;
	private Date fechaIngreso;
	private Date fechaBaja;
	private Usuario usuario;
	private Date ultimaActualizacion;
	private String calle;
	private String numeroCalle;
	private String codigoPostal;
	private String ciudad;
	public String getNombreUno() {
		return nombreUno;
	}
	public String getNombreDos() {
		return nombreDos;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public String getRfc() {
		return rfc;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public String getCalle() {
		return calle;
	}
	public String getNumeroCalle() {
		return numeroCalle;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setNombreUno(String nombreUno) {
		this.nombreUno = nombreUno;
	}
	public void setNombreDos(String nombreDos) {
		this.nombreDos = nombreDos;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public void setNumeroCalle(String numeroCalle) {
		this.numeroCalle = numeroCalle;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	
}
