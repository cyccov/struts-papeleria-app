package mx.mundodafne.dev.business;

import java.util.List;

import mx.mundodafne.dev.entity.Usuario;
import mx.mundodafne.dev.vo.UsuarioVO;

public interface UsuarioBusiness {
	public void altaUsuario(UsuarioVO usuarioVO);
	public List<Usuario> obtieneLosUsuarios();
	public List<Usuario> busquedaPorNombre(String nombreUsuario);
	public Usuario busquedaPorId(long idUsuario);
}
