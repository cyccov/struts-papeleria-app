package mx.mundodafne.dev.business;

import java.util.List;

import mx.mundodafne.dev.dao.ArticuloDAO;
import mx.mundodafne.dev.entity.Articulo;
import mx.mundodafne.dev.entity.ArticuloDetalle;

public class ArticuloBusinessImpl implements ArticuloBusiness {
	private ArticuloDAO articuloDAO = null;
	public long obtieneIDArticulo(Articulo articulo) {
		return articuloDAO.obtieneIDArticulo(articulo);
	}

	public ArticuloDetalle obtieneDetalleArticulo() {
		return articuloDAO.obtieneDetalleArticulo();
	}

	public List<ArticuloDetalle> obtieneDetallesArticulos() {
		return articuloDAO.obtieneDetallesArticulos();
	}

}
