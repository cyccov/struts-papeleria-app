package mx.mundodafne.dev.business;

import java.util.List;

import mx.mundodafne.dev.entity.RolUsuario;
import mx.mundodafne.dev.exceptions.BusinessLayerException;
import mx.mundodafne.dev.vo.RolUsuarioVO;

public interface RolUsuarioBusiness {
	public void altaRolUsuario(RolUsuarioVO rolUsuarioVO) throws BusinessLayerException;
	public List<RolUsuario> obtieneRolesUsuario() throws BusinessLayerException;
	public void eliminaRolUsuario(long rolUsuarioId) throws BusinessLayerException;
	public List<String> obtieneTodosLosRoles() throws BusinessLayerException;
	public void actualizaRolUsuario(long id) throws BusinessLayerException;		
	public long obtieneIdDescripcionRol(String descripcionRolUsuario) throws BusinessLayerException;
	public RolUsuario obtieneRolUsuario(long id) throws BusinessLayerException;
}
