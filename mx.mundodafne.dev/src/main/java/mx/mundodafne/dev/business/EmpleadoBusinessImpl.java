package mx.mundodafne.dev.business;

import java.util.List;

import mx.mundodafne.dev.dao.EmpleadoDAO;
import mx.mundodafne.dev.entity.Empleado;
import mx.mundodafne.dev.vo.EmpleadoVO;

public class EmpleadoBusinessImpl implements EmpleadoBusiness {
	private EmpleadoDAO empleadoDAO = null;
	public List<Empleado> consultaTodosLosEmpleados() {
		return empleadoDAO.consultaTodosLosEmpleados();
	}

	public Empleado consultaEmpleadoPorId(long idEmpleado) {
		return empleadoDAO.consultaEmpleadoPorId(idEmpleado);
	}

	public long obtieneIdEmpleado() {
		return empleadoDAO.obtieneIdEmpleado();
	}

	public void eliminaEmpleadoPorId(long idEmpleado) {
		empleadoDAO.eliminaEmpleadoPorId(idEmpleado);
	}

	public void agregaNuevoEmpleado(EmpleadoVO empleadoVO) {
		empleadoDAO.agregaNuevoEmpleado(empleadoVO);
	}
}