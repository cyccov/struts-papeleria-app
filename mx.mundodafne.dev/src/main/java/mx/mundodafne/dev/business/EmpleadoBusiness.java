package mx.mundodafne.dev.business;

import java.util.List;

import mx.mundodafne.dev.entity.Empleado;
import mx.mundodafne.dev.vo.EmpleadoVO;

public interface EmpleadoBusiness {
	public List<Empleado> consultaTodosLosEmpleados();
	public Empleado consultaEmpleadoPorId(long idEmpleado);
	public long obtieneIdEmpleado();
	public void eliminaEmpleadoPorId(long idEmpleado); 
	public void agregaNuevoEmpleado(EmpleadoVO empleadoVO);
}
