package mx.mundodafne.dev.business;

import java.util.List;

import org.apache.log4j.Logger;

import mx.mundodafne.dev.dao.RolUsuarioDAO;
import mx.mundodafne.dev.dao.RolUsuarioDAOImpl;
import mx.mundodafne.dev.dao.UsuarioDAOImpl;
import mx.mundodafne.dev.entity.RolUsuario;
import mx.mundodafne.dev.exceptions.BusinessLayerException;
import mx.mundodafne.dev.vo.RolUsuarioVO;

public class RolUsuarioBusinessImpl implements RolUsuarioBusiness {
	private final RolUsuarioDAO rolUsuarioDAO = new RolUsuarioDAOImpl();
	private static final Logger logger = Logger.getLogger(UsuarioDAOImpl.class);
	
	public List<RolUsuario> obtieneRolesUsuario() throws BusinessLayerException {
		try {
			return rolUsuarioDAO.obtieneRolesUsuario();	
		} catch(Exception e) {
			throw new BusinessLayerException("Error en capa Business -> funcion obtieneRolesUsuario", e);
		}
	}

	public void altaRolUsuario(RolUsuarioVO rolUsuarioVO) throws BusinessLayerException {
		try {
			rolUsuarioDAO.altaRolUsuario(rolUsuarioVO);	
		} catch (Exception e) {
			throw new BusinessLayerException("Error en capa Business -> funcion altaRolUsuario", e);
		}
	}

	public void eliminaRolUsuario(long rolUsuarioId) throws BusinessLayerException{
		try {
			rolUsuarioDAO.eliminaRolUsuario(rolUsuarioId);
		} catch(Exception e) {
			throw new BusinessLayerException("Error en método eliminaRolUsuario",e);
		}				
	}

	public List<String> obtieneTodosLosRoles() throws BusinessLayerException{
		try {
			return rolUsuarioDAO.obtieneTodosLosRoles();	
		} catch(Exception e) {
			throw new BusinessLayerException("Error en método obtieneTodosLosRoles",e);
		}		
		
	}

	public void actualizaRolUsuario(long id) throws BusinessLayerException{
		try {
			rolUsuarioDAO.actualizaRolUsuario(id);	
		} catch(Exception e) {
			throw new BusinessLayerException("Error en método actualizaRolUsuario",e);
		}		
		
	}

	public long obtieneIdDescripcionRol(String descripcionRolUsuario) throws BusinessLayerException{
		try {
			return rolUsuarioDAO.obtieneIdDescripcionRol(descripcionRolUsuario);		
		} catch(Exception e) {
			throw new BusinessLayerException("Error en método obtieneIdDescripcionRol",e);
		}				
	}

	public RolUsuario obtieneRolUsuario(long id) throws BusinessLayerException{
		RolUsuario rol = new RolUsuario();
		try {
			rol = rolUsuarioDAO.obtieneRolUsuario(id);
		} catch(Exception e) {
			throw new BusinessLayerException("Error en método obtieneRolUsuario",e);
		}		
		return rol;
	}
}