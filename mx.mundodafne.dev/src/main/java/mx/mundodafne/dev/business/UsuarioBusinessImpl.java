package mx.mundodafne.dev.business;

import java.util.List;

import mx.mundodafne.dev.dao.UsuarioDAO;
import mx.mundodafne.dev.dao.UsuarioDAOImpl;
import mx.mundodafne.dev.entity.Usuario;
import mx.mundodafne.dev.vo.UsuarioVO;

public class UsuarioBusinessImpl implements UsuarioBusiness {
	private static final UsuarioDAO usuarioDAO = new UsuarioDAOImpl();
	
	public void altaUsuario(UsuarioVO usuarioVO) {
		usuarioDAO.altaUsuario(usuarioVO);		
	}

	public List<Usuario> obtieneLosUsuarios() {
		// TODO Auto-generated method stub
		return usuarioDAO.obtieneLosUsuarios();
	}

	public List<Usuario> busquedaPorNombre(String nombreUsuario) {
		return usuarioDAO.busquedaPorNombre(nombreUsuario);
	}

	public Usuario busquedaPorId(long idUsuario) {
		return usuarioDAO.busquedaPorId(idUsuario);
	}
 
}
