package mx.mundodafne.dev.business;

import java.util.List;

import mx.mundodafne.dev.entity.Articulo;
import mx.mundodafne.dev.entity.ArticuloDetalle;

public interface ArticuloBusiness {
	public long obtieneIDArticulo(Articulo articulo);
	public ArticuloDetalle obtieneDetalleArticulo();
	public List<ArticuloDetalle> obtieneDetallesArticulos();
}
