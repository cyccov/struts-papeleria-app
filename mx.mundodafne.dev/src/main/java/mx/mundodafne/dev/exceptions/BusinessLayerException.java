package mx.mundodafne.dev.exceptions;

public class BusinessLayerException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6759120190597571224L;

	public BusinessLayerException(String errorMessage, Throwable throwable) {
		super(errorMessage, throwable);
	}

	public BusinessLayerException(String errorMessage) {
		super(errorMessage);
	}
	
}
