package mx.mundodafne.dev.util;

public class Constantes {
	public static final String MAP_EMPLEADOS_SUCCESS = "empleado.success";
	public static final String MAP_EMPLEADOS_ERROR = "empleado.error";
	public static final String MAP_USUARIOS_ALTA_SUCCESS = "alta.usuarios.success";
	public static final String MAP_USUARIOS_ALTA_ERROR = "alta.usuarios.error";
	public static final String FORW_CAT_USUARIOS_SUCCESS = "catalogo.usuarios.success";
	public static final String EMPTY_STRING = "";
	public static final String MAP_USUARIOS_CONSULTA_SUCCESS = "consulta.usuarios.success";
	public static final String MAP_USUARIOS_CONSULTA_ERROR = "consulta.usuarios.error";
	public static final String MAP_ROL_USUARIO_SUCCESS="rol.usuario.success";
	public static final String MAP_ROL_USUARIO_ERROR="rol.usuario.error";
	public static final String MAP_PROVEEDOR_SUCCESS = "proveedor.success";
	public static final String MAP_PROVEEDOR_ERROR = "proveedor.error";
	public static final String PATRON_FECHA_MMDDYYYY = "dd/MM/yyyy";
	public static final String ERROR_ROL_USUARIO_DESCRIPCION_ROL_MISSING = "rol.usuario.descripcionrol.missing";
}
