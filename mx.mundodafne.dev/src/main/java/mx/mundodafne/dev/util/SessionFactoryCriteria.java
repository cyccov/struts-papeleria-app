package mx.mundodafne.dev.util;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import mx.mundodafne.dev.action.UsuarioAction;

public class SessionFactoryCriteria {
	private static SessionFactory sessionFactory = null;
	private static final Logger logger = Logger.getLogger(SessionFactoryCriteria.class);
	
	public static SessionFactory getSessionFactory() {
		try {
			logger.debug("Creando SessionFactory...");
			sessionFactory = new Configuration().configure().buildSessionFactory();
		} catch(HibernateException e) {
			logger.debug("Error creando SessionFactory...",e);
		}
		return sessionFactory;		
	}
}