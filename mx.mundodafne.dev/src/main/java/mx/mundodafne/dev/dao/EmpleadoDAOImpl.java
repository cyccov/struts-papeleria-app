package mx.mundodafne.dev.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import mx.mundodafne.dev.entity.Empleado;
import mx.mundodafne.dev.util.SessionFactoryCriteria;
import mx.mundodafne.dev.vo.EmpleadoVO;

public class EmpleadoDAOImpl implements EmpleadoDAO {
	private static final Logger logger = Logger.getLogger(EmpleadoDAOImpl.class);
	
	
	public void agregaNuevoEmpleado(EmpleadoVO empleadoVO) {
		Empleado empleadoEntity = new Empleado();
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		try {
			logger.info("Iniciando transaccion para agrear nuevo empleado..");
			transaction = session.beginTransaction();
			empleadoEntity.setNombreUno(empleadoVO.getNombreUno());
			session.save(empleadoEntity);
			transaction.commit();
		} catch(Exception e) {
			logger.error("Error en metodo agregaNuevoEmpleado: ",e);
			if (transaction != null) {
				transaction.rollback();
			}			
		} finally {
			session.close();
		}		
	}

	public List<Empleado> consultaTodosLosEmpleados() {
		// TODO Auto-generated method stub
		return null;
	}

	public Empleado consultaEmpleadoPorId(long idEmpleado) {
		// TODO Auto-generated method stub
		return null;
	}

	public long obtieneIdEmpleado() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void eliminaEmpleadoPorId(long idEmpleado) {
		// TODO Auto-generated method stub
		
	}

}
