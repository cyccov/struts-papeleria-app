package mx.mundodafne.dev.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import mx.mundodafne.dev.entity.RolUsuario;
import mx.mundodafne.dev.util.SessionFactoryCriteria;
import mx.mundodafne.dev.vo.RolUsuarioVO;

public class RolUsuarioDAOImpl implements RolUsuarioDAO {

	private final RolUsuario rolUsuarioEntity = new RolUsuario();
	private static final Logger logger = Logger.getLogger(RolUsuarioDAOImpl.class);
	
	
	
	public RolUsuario obtieneRolUsuario(long id) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();		
		Transaction transaction = null;
		RolUsuario resultado = new RolUsuario();
		try {
			transaction = session.beginTransaction();
			Criteria criteria = session.createCriteria(RolUsuario.class);
			criteria.add(Restrictions.eq("idRolUsuario", id));
			resultado = (RolUsuario) criteria.uniqueResult();
			transaction.commit();
		} catch(HibernateException e) {
			logger.error("Error en metodo obtieneRolUsuario: ",e);
			if (transaction != null) {
				transaction.rollback();
			}
		} catch(Exception e) {
			logger.error("Error en metodo obtieneRolUsuario: ",e);
			if (transaction != null) {
				transaction.rollback();
			}			
		} finally {
			session.close();
		}		
		return resultado;
	}

	public void altaRolUsuario(RolUsuarioVO rolUsuarioVO) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		
		try {
			logger.info("Iniciando metodo altaRolUsuario");
			transaction = session.beginTransaction();
			rolUsuarioEntity.setDescripcionRol(rolUsuarioVO.getDescripcionRol());
			rolUsuarioEntity.setNivelPermiso(rolUsuarioVO.getNivelPermiso());
			session.save(rolUsuarioEntity);
			transaction.commit();
		} catch(HibernateException e) {
			logger.error("Error en metodo altaRolUsuario: ",e);
			if (transaction != null) {
				transaction.rollback();	
			}
		} finally {
			session.close();
		}
	}

	public List<RolUsuario> obtieneRolesUsuario() {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();		
		Transaction transaction = null;
		List<RolUsuario> resultados = new ArrayList<RolUsuario>();
		try {
			transaction = session.beginTransaction();
			Criteria criteria = session.createCriteria(RolUsuario.class);
			criteria.setProjection(Projections.property("descripcionRol"));
			resultados = criteria.list();
			transaction.commit();
		} catch(HibernateException e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) {
				transaction.rollback();
			}
		} catch(Exception e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) {
				transaction.rollback();
			}			
		} finally {
			session.close();
		}		
		return resultados;		
	}

	public void eliminaRolUsuario(long rolUsuarioId) {
		// TODO Auto-generated method stub

	}

	public List<String> obtieneTodosLosRoles() throws Exception{
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();		
		Transaction transaction = null;
		List<String> resultados = new ArrayList<String>();
		try {
			transaction = session.beginTransaction();
			Criteria criteria = session.createCriteria(RolUsuario.class);
			criteria.setProjection(Projections.property("descripcionRol"));
			resultados = criteria.list();
			transaction.commit();
		} catch(HibernateException e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) {
				transaction.rollback();
			}
			throw new Exception("Ocurrio un error: "+e.getMessage(),e);
		} catch(Exception e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) {
				transaction.rollback();
			}			
			throw new Exception("Ocurrio un error: "+e.getMessage(),e);
		} finally {
			session.close();
		}		
		return resultados;
	}

	public void actualizaRolUsuario(long id) {
		// TODO Auto-generated method stub

	}

	public long obtieneIdDescripcionRol(String descripcionRolUsuario) throws Exception {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();		
		Transaction transaction = null;
		long idRolUsuario = 0l;
		try {
			transaction = session.beginTransaction();
			Criteria criteria = session.createCriteria(RolUsuario.class);
			criteria.add(Restrictions.eq("descripcionRol", descripcionRolUsuario));
			criteria.setProjection(Projections.property("idRolUsuario"));
			idRolUsuario = (Long)criteria.uniqueResult();
			transaction.commit();
		} catch(HibernateException e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) {
				transaction.rollback();
			}
			throw new Exception("Ocurrio un error: "+e.getMessage(),e);
		} catch(Exception e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) {
				transaction.rollback();
			}			
			throw new Exception("Ocurrio un error: "+e.getMessage(),e);
		} finally {
			session.close();
		}		
		return idRolUsuario;
	}

	
}
