package mx.mundodafne.dev.dao;

import java.util.List;

import mx.mundodafne.dev.entity.Proveedor;
import mx.mundodafne.dev.vo.ProveedorVO;

public interface ProveedorDAO {
	public List<Proveedor> consultaTodosLosProveedores();
	public List<Proveedor> busquedaProveedoresPorRazonSocialORfc(String razonSocial, String rfcProveedor);
	public Proveedor busquedaProveedorPorId(long idProveedor);
	public long obtieneIdProveedorPorRfc(String rfcProveedor);
	public void altaProveedor(ProveedorVO proveedorVO);
	public void actualizaProveedor(ProveedorVO proveedorVO);
	public void bajaProveedor(ProveedorVO proveedorVO);
}
