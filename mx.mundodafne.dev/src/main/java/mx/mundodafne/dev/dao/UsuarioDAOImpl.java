package mx.mundodafne.dev.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import mx.mundodafne.dev.business.RolUsuarioBusiness;
import mx.mundodafne.dev.business.RolUsuarioBusinessImpl;
import mx.mundodafne.dev.entity.Usuario;
import mx.mundodafne.dev.exceptions.BusinessLayerException;
import mx.mundodafne.dev.util.SessionFactoryCriteria;
import mx.mundodafne.dev.vo.UsuarioVO;

public class UsuarioDAOImpl implements UsuarioDAO{
	private static final Logger logger = Logger.getLogger(UsuarioDAOImpl.class);
	private final Usuario usuarioEntity = new Usuario();
	
	public void altaUsuario(UsuarioVO usuarioVO) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		RolUsuarioBusiness rolUsuarioBusiness = new RolUsuarioBusinessImpl();
		long idRolUsuario = 0l;
		try {
			logger.info("Iniciando DAOUsuario metodo altaUsuario...");
			transaction = session.beginTransaction();
			usuarioEntity.setNombreUsuario(usuarioVO.getNombreUsuario());
			usuarioEntity.setUsuarioPass(usuarioVO.getPasswordUsuario());
			idRolUsuario = rolUsuarioBusiness.obtieneIdDescripcionRol(usuarioVO.getRolUsuario());
			usuarioEntity.setRolUsuario(rolUsuarioBusiness.obtieneRolUsuario(idRolUsuario));
			session.save(usuarioEntity);
			usuarioVO.setStatusAlta(true);
			usuarioVO.setMensajeAlta("ALTA EXITOSA DE USUARIO");
			transaction.commit();
		} catch(HibernateException e) {
			if (transaction != null) {
				transaction.rollback();	
			}
			logger.error("Error metodo altaUsuario, clase UsuarioDAO: ",e);
		} catch (BusinessLayerException e) {
			logger.error("Error: método altaUsuario",e);
		}finally {
			session.close();
		}
	}

	public List<Usuario> obtieneLosUsuarios() {
		logger.info("Recuperando usuarios registrados...");
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();		
		Transaction transaction = null;
		List<Usuario> resultados = new ArrayList<Usuario>();
		try {
			transaction = session.beginTransaction();
			Criteria criteria = session.createCriteria(Usuario.class);
			resultados = criteria.list();
			transaction.commit();
		} catch(HibernateException e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) { 
				transaction.rollback();
			}
		} catch(Exception e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) {
				transaction.rollback();
			}			
		} finally {
			session.close();
		}		
		return resultados;
	}

	public List<Usuario> busquedaPorNombre(String nombreUsuario) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;		
		Criteria criteria = session.createCriteria(Usuario.class);
		List<Usuario> resultados = new ArrayList();
		try {
			transaction = session.beginTransaction();
			criteria.add(Restrictions.like("nombreUsuario", nombreUsuario)); //se hace filtro del nombre de usuario
			resultados = criteria.list();
			transaction.commit();
		} catch(HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} catch(Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return resultados;
	}

	public Usuario busquedaPorId(long idUsuario) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;		
		Criteria criteria = session.createCriteria(Usuario.class);
		Usuario resultadoBusqueda = new Usuario();
		try {
			transaction = session.beginTransaction();
			criteria.add(Restrictions.like("idUsuario", idUsuario)); //se hace filtro por ID de usuario
			resultadoBusqueda = (Usuario) criteria.uniqueResult(); //convierte resultado Criteria (objeto) a tipo Usuario (entity)
			transaction.commit();			
		} catch(HibernateException e) {
			logger.error("Error en metodo busquedaPorId: ",e);
			if (transaction != null) {
				transaction.rollback();
			}			
		} catch(Exception e) {
			logger.error("Error en metodo busquedaPorId: ",e);
			if (transaction != null) {
				transaction.rollback();
			}			
		} finally {
			session.close();
		}		
		return resultadoBusqueda;
	}
}