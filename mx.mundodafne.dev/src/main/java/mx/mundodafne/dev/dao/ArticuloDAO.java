package mx.mundodafne.dev.dao;

import java.util.List;

import mx.mundodafne.dev.entity.Articulo;
import mx.mundodafne.dev.entity.ArticuloDetalle;

public interface ArticuloDAO {
	public long obtieneIDArticulo(Articulo articulo);
	public ArticuloDetalle obtieneDetalleArticulo();
	public List<ArticuloDetalle> obtieneDetallesArticulos();
}
