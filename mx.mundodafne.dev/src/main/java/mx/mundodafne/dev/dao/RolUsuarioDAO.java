package mx.mundodafne.dev.dao;

import java.util.List;

import mx.mundodafne.dev.entity.RolUsuario;
import mx.mundodafne.dev.vo.RolUsuarioVO;

public interface RolUsuarioDAO {
	public void altaRolUsuario(RolUsuarioVO rolUsuarioVO);
	public List<RolUsuario> obtieneRolesUsuario();
	public void eliminaRolUsuario(long rolUsuarioId);
	public List<String> obtieneTodosLosRoles() throws Exception;
	public void actualizaRolUsuario(long id);
	public long obtieneIdDescripcionRol(String descripcionRolUsuario) throws Exception;
	public RolUsuario obtieneRolUsuario(long id);
}
