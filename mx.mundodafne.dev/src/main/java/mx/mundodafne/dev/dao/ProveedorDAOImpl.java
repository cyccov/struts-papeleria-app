package mx.mundodafne.dev.dao;

import static mx.mundodafne.dev.util.Constantes.PATRON_FECHA_MMDDYYYY;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import mx.mundodafne.dev.entity.Proveedor;
import mx.mundodafne.dev.util.SessionFactoryCriteria;
import mx.mundodafne.dev.vo.ProveedorVO;

public class ProveedorDAOImpl implements ProveedorDAO {
	private static final Logger logger = Logger.getLogger(ProveedorDAOImpl.class);
	private Proveedor proveedorEntity = new Proveedor();
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(PATRON_FECHA_MMDDYYYY);
	private static DateFormat df = simpleDateFormat;
	private static Date fechaActualizacion;
	
	
	public List<Proveedor> consultaTodosLosProveedores() {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();		
		Transaction transaction = null;
		List<Proveedor> resultados = new ArrayList<Proveedor>();
		try {
			transaction = session.beginTransaction();
			Criteria criteria = session.createCriteria(Proveedor.class);
			resultados = criteria.list();
			transaction.commit();
		} catch(HibernateException e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) {
				transaction.rollback();
			}
		} catch(Exception e) {
			logger.error("Error en metodo obtieneLosUsuarios: ",e);
			if (transaction != null) {
				transaction.rollback();
			}			
		} finally {
			session.close();
		}		
		return resultados;
	}

	public List<Proveedor> busquedaProveedoresPorRazonSocialORfc(String razonSocial, String rfcProveedor) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;		
		Criteria criteria = session.createCriteria(Proveedor.class);
		List<Proveedor> resultados = new ArrayList();
		
		try {
			transaction = session.beginTransaction();
			criteria.add(Restrictions.like("razonSocial", razonSocial));
			criteria.add(Restrictions.like("rfcProveedor", rfcProveedor));
			resultados = criteria.list();
			transaction.commit();			
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} catch (NullPointerException e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		
		return resultados;
	}

	public Proveedor busquedaProveedorPorId(long idProveedor) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;	
		ProveedorVO proveedorVO = new ProveedorVO();
		Criteria criteria = session.createCriteria(Proveedor.class);
		try{
			transaction = session.beginTransaction();
			criteria.add(Restrictions.like("idProveedor", idProveedor));
			proveedorVO.setProveedor((Proveedor)criteria.uniqueResult());
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return proveedorVO.getProveedor();		
	}

	public long obtieneIdProveedorPorRfc(String rfcProveedor) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;	
		ProveedorVO proveedorVO = new ProveedorVO();
		Criteria criteria = session.createCriteria(Proveedor.class);
		try{
			transaction = session.beginTransaction();
			criteria.add(Restrictions.like("rfcProveedor", rfcProveedor));
			criteria.setProjection(Projections.property("idProveedor"));
			proveedorVO.setIdProveedor((Long)criteria.uniqueResult());
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return proveedorVO.getIdProveedor();
	}

	public void altaProveedor(ProveedorVO proveedorVO) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;

		try {
			logger.info("Iniciando metodo altaProveedor...");
			transaction = session.beginTransaction();
			proveedorEntity.setDireccion(proveedorVO.getDireccion());
			proveedorEntity.setRazonSocial(proveedorVO.getRazonSocial());
			proveedorEntity.setRfcProveedor(proveedorVO.getRfcProveedor());
			proveedorEntity.setFechaAlta(proveedorVO.getFechaAlta());
			session.save(proveedorEntity);
			proveedorVO.setMensaje("Proveedor dado de alta exitosamente");
			transaction.commit();
			logger.info(proveedorVO.getMensaje());
		} catch (HibernateException e) {
			logger.error("Error en metodo altaProveedor... ",e);
			if (transaction != null) {
				transaction.rollback();	
			}
		} catch (Exception e) {
			logger.error("Error en metodo altaProveedor... ",e);
			if (transaction != null) {
				transaction.rollback();	
			}			
		} finally {
			session.close();
		}
	}

	public void actualizaProveedor(ProveedorVO proveedorVO) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		Criteria criteria = session.createCriteria(Proveedor.class);
		try {
			logger.info("Iniciando metodo altaProveedor...");
			fechaActualizacion = df.parse(simpleDateFormat.format(new Date()));
			transaction = session.beginTransaction();
			proveedorEntity.setDireccion(proveedorVO.getDireccion());
			proveedorEntity.setRazonSocial(proveedorVO.getRazonSocial());
			proveedorEntity.setRfcProveedor(proveedorVO.getRfcProveedor());
			proveedorEntity.setUltimaActualizacion(fechaActualizacion);
			session.saveOrUpdate(proveedorEntity);
			transaction.commit();
		} catch (HibernateException e) {
			logger.error("Error en metodo altaProveedor... ",e);
			if (transaction != null) {
				transaction.rollback();	
			}
		} catch (ParseException e) {
			logger.error("Error con fecha de actualizacion: ",e);
		}catch (Exception e) {
			logger.error("Error en metodo altaProveedor... ",e);
			if (transaction != null) {
				transaction.rollback();	
			}			
		} finally {
			session.close();
		}
	}

	public void bajaProveedor(ProveedorVO proveedorVO) {
		SessionFactory factory = SessionFactoryCriteria.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = null;
		Criteria criteria = session.createCriteria(Proveedor.class);
		Date fechaBaja;
		try {
			logger.info("Iniciando metodo bajaProveedor...");
			fechaBaja = df.parse(simpleDateFormat.format(new Date()));
			transaction = session.beginTransaction();
			proveedorEntity.setFechaBaja(fechaBaja);
			session.saveOrUpdate(proveedorEntity);
			transaction.commit();
		} catch (HibernateException e) {
			logger.error("Error en metodo bajaProveedor... ",e);
			if (transaction != null) {
				transaction.rollback();	
			}
		} catch (ParseException e) {
			logger.error("Error con fecha de baja: ",e);
		}catch (Exception e) {
			logger.error("Error en metodo bajaProveedor... ",e);
			if (transaction != null) {
				transaction.rollback();	
			}			
		} finally {
			session.close();
		}				
	}

}
