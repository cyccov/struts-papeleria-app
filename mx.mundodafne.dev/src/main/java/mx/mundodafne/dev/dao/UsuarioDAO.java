package mx.mundodafne.dev.dao;

import java.util.List;

import mx.mundodafne.dev.entity.Usuario;
import mx.mundodafne.dev.vo.UsuarioVO;

/**
 * CAPA PARA PERSISTENCIA DE DATOS PARA USUARIO
 * */
public interface UsuarioDAO {
	
	public void altaUsuario(UsuarioVO usuarioVO);
	public List<Usuario> obtieneLosUsuarios();
	public List<Usuario> busquedaPorNombre(String nombreUsuario);
	public Usuario busquedaPorId(long idUsuario);
}
