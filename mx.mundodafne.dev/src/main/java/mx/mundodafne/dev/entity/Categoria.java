package mx.mundodafne.dev.entity;

import java.util.Date;
import java.util.Set;

public class Categoria {
	
	private long idCategoria;
	private Subcategoria subcategoria;
	private String descripcionCategoria;
	private Date ultimaActualizacion;
	private Set subcategorias;
	
	public Set getSubcategorias() {
		return subcategorias;
	}
	public void setSubcategorias(Set subcategorias) {
		this.subcategorias = subcategorias;
	}
	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	public long getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(long idCategoria) {
		this.idCategoria = idCategoria;
	}
	public Subcategoria getSubcategoria() {
		return subcategoria;
	}
	public void setSubcategoria(Subcategoria subcategoria) {
		this.subcategoria = subcategoria;
	}
	public String getDescripcionCategoria() {
		return descripcionCategoria;
	}
	public void setDescripcionCategoria(String descripcionCategoria) {
		this.descripcionCategoria = descripcionCategoria;
	}
	
	
}
