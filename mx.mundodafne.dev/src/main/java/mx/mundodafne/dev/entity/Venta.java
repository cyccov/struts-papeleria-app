package mx.mundodafne.dev.entity;

public class Venta {
	private long idVenta;
	private VentaDetalle ventaDetalle;
	
	public long getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(long idVenta) {
		this.idVenta = idVenta;
	}
	public VentaDetalle getVentaDetalle() {
		return ventaDetalle;
	}
	public void setVentaDetalle(VentaDetalle ventaDetalle) {
		this.ventaDetalle = ventaDetalle;
	}
	
	
}
