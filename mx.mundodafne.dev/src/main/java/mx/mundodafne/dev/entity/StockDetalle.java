package mx.mundodafne.dev.entity;

import java.util.Date;

public class StockDetalle {
	private long idStockDetalle;
	private Date fechaEntrada;
	private Date ultimaActualizacion;
	private int cantidadInicial;
	private int cantidadFinal;
	
	
	
	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	public long getIdStockDetalle() {
		return idStockDetalle;
	}
	public void setIdStockDetalle(long idStockDetalle) {
		this.idStockDetalle = idStockDetalle;
	}
	public Date getFechaEntrada() {
		return fechaEntrada;
	}
	public void setFechaEntrada(Date fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}
	public int getCantidadInicial() {
		return cantidadInicial;
	}
	public void setCantidadInicial(int cantidadInicial) {
		this.cantidadInicial = cantidadInicial;
	}
	public int getCantidadFinal() {
		return cantidadFinal;
	}
	public void setCantidadFinal(int cantidadFinal) {
		this.cantidadFinal = cantidadFinal;
	}
	
	
}
