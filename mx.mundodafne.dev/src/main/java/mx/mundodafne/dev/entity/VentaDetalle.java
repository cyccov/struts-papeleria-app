package mx.mundodafne.dev.entity;

import java.util.Date;

public class VentaDetalle extends Venta{
	private long idVentaDetalle;
	private Date fechaVenta;
	private Empleado empleado;
	private ArticuloDetalle articuloDetalle;
	private Cliente cliente;
	
	public long getIdVentaDetalle() {
		return idVentaDetalle;
	}
	public void setIdVentaDetalle(long idVentaDetalle) {
		this.idVentaDetalle = idVentaDetalle;
	}
	public Date getFechaVenta() {
		return fechaVenta;
	}
	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}
	public Empleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
	public ArticuloDetalle getArticuloDetalle() {
		return articuloDetalle;
	}
	public void setArticuloDetalle(ArticuloDetalle articuloDetalle) {
		this.articuloDetalle = articuloDetalle;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
}
