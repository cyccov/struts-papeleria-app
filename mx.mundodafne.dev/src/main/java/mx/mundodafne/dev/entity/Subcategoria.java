package mx.mundodafne.dev.entity;

import java.util.Date;

public class Subcategoria {
	private long idSubcategoria;
	private String descripcionSubcategoria;
	private Categoria categoria;
	private Date ultimaActualizacion;
	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	public long getIdSubcategoria() {
		return idSubcategoria;
	}
	public void setIdSubcategoria(long idSubcategoria) {
		this.idSubcategoria = idSubcategoria;
	}
	public String getDescripcionSubcategoria() {
		return descripcionSubcategoria;
	}
	public void setDescripcionSubcategoria(String descripcionSubcategoria) {
		this.descripcionSubcategoria = descripcionSubcategoria;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	
}
