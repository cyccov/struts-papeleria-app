package mx.mundodafne.dev.entity;

public class Usuario {
	private long idUsuario;
	private String nombreUsuario;
	private String usuarioPass;
	private RolUsuario rolUsuario;
	private Empleado empleado;
		
	public Empleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getUsuarioPass() {
		return usuarioPass;
	}
	public void setUsuarioPass(String usuarioPass) {
		this.usuarioPass = usuarioPass;
	}
	public RolUsuario getRolUsuario() {
		return rolUsuario;
	}
	public void setRolUsuario(RolUsuario rolUsuario) {
		this.rolUsuario = rolUsuario;
	}	
}