package mx.mundodafne.dev.entity;

import java.util.Date;

public class RolUsuario {

	private long idRolUsuario;
	private String descripcionRol;
	private int nivelPermiso;
	private Date ultimaActualizacion;
	
	public long getIdRolUsuario() {
		return idRolUsuario;
	}
	public String getDescripcionRol() {
		return descripcionRol;
	}
	public int getNivelPermiso() {
		return nivelPermiso;
	}
	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}

	public void setIdRolUsuario(long idRolUsuario) {
		this.idRolUsuario = idRolUsuario;
	}
	public void setDescripcionRol(String descripcionRol) {
		this.descripcionRol = descripcionRol;
	}
	public void setNivelPermiso(int nivelPermiso) {
		this.nivelPermiso = nivelPermiso;
	}
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
}