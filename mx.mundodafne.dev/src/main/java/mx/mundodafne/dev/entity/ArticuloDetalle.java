package mx.mundodafne.dev.entity;

import java.util.Date;
import java.util.Set;

public class ArticuloDetalle extends Articulo{
	
	private long idArticuloDetalle;
	private float precioUnitario;
	private String fechaAlta;
	private Categoria categoria;
	private Subcategoria subcategoria;
	private StockDetalle stockDetalle;
	private Date ultimaActualizacion;
	private Date fechaEntrada;
	private int cantidadInicial;
	private int cantidadFinal;
	
	private Set<Categoria> categorias;
	private Set<Subcategoria> subcategorias;
//	private Set<StockDetalle> stockDetalles;

	
	
	
	public Date getFechaEntrada() {
		return fechaEntrada;
	}
	public int getCantidadInicial() {
		return cantidadInicial;
	}
	public int getCantidadFinal() {
		return cantidadFinal;
	}
	public void setFechaEntrada(Date fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}
	public void setCantidadInicial(int cantidadInicial) {
		this.cantidadInicial = cantidadInicial;
	}
	public void setCantidadFinal(int cantidadFinal) {
		this.cantidadFinal = cantidadFinal;
	}
	public Set<Categoria> getCategorias() {
		return categorias;
	}
	public void setCategorias(Set<Categoria> categorias) {
		this.categorias = categorias;
	}
	public Set<Subcategoria> getSubcategorias() {
		return subcategorias;
	}
	public void setSubcategorias(Set<Subcategoria> subcategorias) {
		this.subcategorias = subcategorias;
	}
//	public Set<StockDetalle> getStockDetalles() {
//		return stockDetalles;
//	}
//	public void setStockDetalles(Set<StockDetalle> stockDetalles) {
//		this.stockDetalles = stockDetalles;
//	}
	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	public long getIdArticuloDetalle() {
		return idArticuloDetalle;
	}
	public void setIdArticuloDetalle(long idArticuloDetalle) {
		this.idArticuloDetalle = idArticuloDetalle;
	}
	public float getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(float precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public Subcategoria getSubcategoria() {
		return subcategoria;
	}
	public void setSubcategoria(Subcategoria subcategoria) {
		this.subcategoria = subcategoria;
	}
	public StockDetalle getStockDetalle() {
		return stockDetalle;
	}
	public void setStockDetalle(StockDetalle stockDetalle) {
		this.stockDetalle = stockDetalle;
	}		
}
