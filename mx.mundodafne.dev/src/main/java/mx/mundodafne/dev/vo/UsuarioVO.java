package mx.mundodafne.dev.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import mx.mundodafne.dev.entity.Usuario;

public class UsuarioVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -64950496483366571L;
	private long usuarioId;
	private String nombreUsuario;
	private String passwordUsuario;
	private List rolesUsuario;
	private String mensajeAlta;
	private boolean statusAlta;
	private List<Usuario> listaUsuarios;
	private Usuario usuario;
	private String rolUsuario;
	
	
	
	public String getRolUsuario() {
		return rolUsuario;
	}
	public void setRolUsuario(String rolUsuario) {
		this.rolUsuario = rolUsuario;
	}
	public long getUsuarioId() {
		return usuarioId;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public String getPasswordUsuario() {
		return passwordUsuario;
	}
	public List getRolesUsuario() {
		return rolesUsuario;
	}
	public String getMensajeAlta() {
		return mensajeAlta;
	}
	public boolean isStatusAlta() {
		return statusAlta;
	}
	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuarioId(long usuarioId) {
		this.usuarioId = usuarioId;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public void setPasswordUsuario(String passwordUsuario) {
		this.passwordUsuario = passwordUsuario;
	}
	public void setRolesUsuario(List rolesUsuario) {
		this.rolesUsuario = rolesUsuario;
	}
	public void setMensajeAlta(String mensajeAlta) {
		this.mensajeAlta = mensajeAlta;
	}
	public void setStatusAlta(boolean statusAlta) {
		this.statusAlta = statusAlta;
	}
	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
	
}
