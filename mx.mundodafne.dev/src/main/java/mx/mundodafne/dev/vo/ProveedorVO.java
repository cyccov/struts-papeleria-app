package mx.mundodafne.dev.vo;

import java.util.Date;
import java.util.List;

import mx.mundodafne.dev.entity.Proveedor;

public class ProveedorVO {
	private static final long serialVersionUID = -7074484793117254435L;
	private long idProveedor;
	private String rfcProveedor;
	private String razonSocial;
	private String direccion;
	private Date fechaAlta;
	private Date fechaBaja;
	private Date ultimaActualizacion;
	private String mensaje;
	private List<Proveedor> listaProveedores;
	private Proveedor proveedor;
	private boolean statusAlta;
	
	public boolean isStatusAlta() {
		return statusAlta;
	}
	public void setStatusAlta(boolean statusAlta) {
		this.statusAlta = statusAlta;
	}
	public long getIdProveedor() {
		return idProveedor;
	}
	public String getRfcProveedor() {
		return rfcProveedor;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public String getDireccion() {
		return direccion;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public List<Proveedor> getListaProveedores() {
		return listaProveedores;
	}
	public Proveedor getProveedor() {
		return proveedor;
	}
	public void setIdProveedor(long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public void setRfcProveedor(String rfcProveedor) {
		this.rfcProveedor = rfcProveedor;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public void setListaProveedores(List<Proveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	
	
}
