package mx.mundodafne.dev.vo;

import java.util.Date;

import mx.mundodafne.dev.entity.Usuario;

public class RolUsuarioVO {
	private long idRolUsuario;
	private String descripcionRol;
	private int nivelPermiso;
	private Date ultimaActualizacion;
	private Usuario usuario;
	public long getIdRolUsuario() {
		return idRolUsuario;
	}
	public String getDescripcionRol() {
		return descripcionRol;
	}
	public int getNivelPermiso() {
		return nivelPermiso;
	}
	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setIdRolUsuario(long idRolUsuario) {
		this.idRolUsuario = idRolUsuario;
	}
	public void setDescripcionRol(String descripcionRol) {
		this.descripcionRol = descripcionRol;
	}
	public void setNivelPermiso(int nivelPermiso) {
		this.nivelPermiso = nivelPermiso;
	}
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
}
