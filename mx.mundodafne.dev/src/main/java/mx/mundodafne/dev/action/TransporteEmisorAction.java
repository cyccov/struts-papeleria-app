package mx.mundodafne.dev.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.mundodafne.dev.form.TransporteForm;

public class TransporteEmisorAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		if (request.getParameter("mandaTransporteEmisor") != null) {
			TransporteForm transporteForm = (TransporteForm) form;
			transporteForm.getValorQueViaja();
			request.setAttribute("mensajeQueViaja", transporteForm.getValorQueViaja());
			return mapping.findForward("transporte.success");
		}
		
		TransporteForm transporteForm = (TransporteForm) form;
		transporteForm.getValorQueViaja();
		request.setAttribute("mensajeQueViaja", transporteForm.getValorQueViaja());
		
		return mapping.findForward("transporte.emisor.success");
	}

}
