package mx.mundodafne.dev.action;

import static mx.mundodafne.dev.util.Constantes.MAP_EMPLEADOS_SUCCESS;
import static mx.mundodafne.dev.util.Constantes.MAP_EMPLEADOS_ERROR;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import mx.mundodafne.dev.dao.EmpleadoDAOImpl;
import mx.mundodafne.dev.form.EmpleadoForm;
import mx.mundodafne.dev.vo.EmpleadoVO;

public class EmpleadoAction extends DispatchAction{
	static final Logger logger = Logger.getLogger(EmpleadoAction.class);
	private EmpleadoVO empleadoVO = new EmpleadoVO();
	private EmpleadoDAOImpl empleadoDAO = new EmpleadoDAOImpl();
	
	protected ActionForward cargaCatalogo(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		EmpleadoForm empleadoForm = (EmpleadoForm) form;
		
		try {
			
		} catch (Exception e) {
			
		}
		
		return mapping.findForward(MAP_EMPLEADOS_SUCCESS);
	}	
	
	protected ActionForward altaEmpleado(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			logger.info("Inicio de alta de empleado: ");
			EmpleadoForm empleadoForm = (EmpleadoForm) form;			
			empleadoVO.setNombreUno(empleadoForm.getNombreUno());
			empleadoVO.setNombreDos(empleadoForm.getNombreDos());
			if (!empleadoForm.getNombreUno().isEmpty() && !empleadoForm.getNombreDos().isEmpty()) {
				empleadoDAO.agregaNuevoEmpleado(empleadoVO);
			} else {
				return mapping.findForward(MAP_EMPLEADOS_SUCCESS);
			}			
			empleadoDAO.agregaNuevoEmpleado(empleadoVO);
		} catch(Exception e) {
			logger.error("Error metodo altaEmpleado: ",e);
			return mapping.findForward(MAP_EMPLEADOS_ERROR);
		}
		return mapping.findForward(MAP_EMPLEADOS_SUCCESS);
	}
	
	protected ActionForward bajaEmpleado(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response, String name) throws Exception {
		// TODO Auto-generated method stub
		return mapping.findForward(MAP_EMPLEADOS_SUCCESS);
	}
	
	protected ActionForward consultaEmpleado(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response, String name) throws Exception {
		// TODO Auto-generated method stub
		return mapping.findForward(MAP_EMPLEADOS_SUCCESS);
	}
	
	protected ActionForward actualizaEmpleado(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response, String name) throws Exception {		
		try {
			
		} catch (Exception e) {
			return mapping.findForward(MAP_EMPLEADOS_ERROR);
		}
		return mapping.findForward(MAP_EMPLEADOS_SUCCESS);
	}
	
}
