package mx.mundodafne.dev.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.mundodafne.dev.form.TransporteForm;

public class TransporteDestinoAction extends Action{

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		TransporteForm transporteForm = (TransporteForm) form;
		String msg = request.getParameter("msg");
		transporteForm.setValorQueViaja("Mensaje que viaja");
		session.setAttribute("session_atributo", 1L);
		return mapping.findForward("transporte.success");		
	}

}
