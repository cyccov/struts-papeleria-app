package mx.mundodafne.dev.action;

import static mx.mundodafne.dev.util.Constantes.EMPTY_STRING;
import static mx.mundodafne.dev.util.Constantes.FORW_CAT_USUARIOS_SUCCESS;
import static mx.mundodafne.dev.util.Constantes.MAP_USUARIOS_ALTA_ERROR;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.mundodafne.dev.business.UsuarioBusiness;
import mx.mundodafne.dev.business.UsuarioBusinessImpl;
import mx.mundodafne.dev.dao.RolUsuarioDAOImpl;
import mx.mundodafne.dev.form.UsuarioForm;
import mx.mundodafne.dev.vo.UsuarioVO;

/**
 * Action para dar de alta a nuevos usuarios
 * */
public class UsuarioAction extends Action{
	final static Logger logger = Logger.getLogger(UsuarioAction.class);
	
	private void reset(UsuarioForm form) {
		form.setNombreUsuario(EMPTY_STRING);
		form.setPasswordUsuario(EMPTY_STRING);
	}
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {				
		try {		
			logger.info("Iniciando clase UsuarioAction");			
			UsuarioForm usuarioForm = (UsuarioForm) form; //definimos el actionform para este action			
			UsuarioVO usuarioVO = new UsuarioVO();
			UsuarioBusiness usuarioBusiness = new UsuarioBusinessImpl();
			usuarioVO.setListaUsuarios(new UsuarioBusinessImpl().obtieneLosUsuarios());
			request.setAttribute("lista_resultados", usuarioVO.getListaUsuarios());
			usuarioVO.setRolesUsuario(new RolUsuarioDAOImpl().obtieneTodosLosRoles());
			request.setAttribute("rolesUsuario", usuarioVO.getRolesUsuario()); //mandamos al request el atributo "rolesUsuario" con informacion contenida en objeto rolesUsuario
			request.setAttribute("roles_usuario_lista", usuarioVO.getRolesUsuario());						
			if (usuarioForm.getNombreUsuario() == null && usuarioForm.getPasswordUsuario() == null) {
				return mapping.findForward(MAP_USUARIOS_ALTA_ERROR);
			} else {				
				usuarioVO.setNombreUsuario(usuarioForm.getNombreUsuario());
				usuarioVO.setPasswordUsuario(usuarioForm.getPasswordUsuario());
				usuarioVO.setRolUsuario(usuarioForm.getRolUsuario());
				logger.info("Rol usuario seleccionado: "+usuarioVO.getRolUsuario());
				usuarioBusiness.altaUsuario(usuarioVO);
				reset(usuarioForm);
			}			
			logger.info(usuarioVO.getMensajeAlta());
		} catch(Exception e) {
			logger.error(e);
			request.setAttribute("errorMsg", e.getMessage());
			return mapping.findForward(MAP_USUARIOS_ALTA_ERROR);
		}
		return mapping.findForward(FORW_CAT_USUARIOS_SUCCESS);
	}	
}