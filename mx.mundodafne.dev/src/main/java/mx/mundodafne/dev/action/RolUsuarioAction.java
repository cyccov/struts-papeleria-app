package mx.mundodafne.dev.action;

import static mx.mundodafne.dev.util.Constantes.EMPTY_STRING;
import static mx.mundodafne.dev.util.Constantes.MAP_ROL_USUARIO_ERROR;
import static mx.mundodafne.dev.util.Constantes.MAP_ROL_USUARIO_SUCCESS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.mundodafne.dev.business.RolUsuarioBusiness;
import mx.mundodafne.dev.business.RolUsuarioBusinessImpl;
import mx.mundodafne.dev.form.RolUsuarioForm;
import mx.mundodafne.dev.form.TransporteForm;
import mx.mundodafne.dev.vo.RolUsuarioVO;

public class RolUsuarioAction extends Action{
	private static final RolUsuarioBusiness rolUsuarioBusiness = new RolUsuarioBusinessImpl();
	private void reset(RolUsuarioForm form) {
		form.setDescripcionRol(EMPTY_STRING);
		form.setNivelPermiso(0);
	}
	
	
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		RolUsuarioForm rolUsuarioForm = (RolUsuarioForm) form;
		RolUsuarioVO rolUsuarioVO = new RolUsuarioVO();		
		HttpSession session = request.getSession();
		try {
			if (request.getParameter("mandaTransporteEmisor") != null) {
				TransporteForm transporteForm = (TransporteForm) form;
				transporteForm.getValorQueViaja();
				request.setAttribute("mensajeQueViaja", transporteForm.getValorQueViaja());
				return mapping.findForward("transporte.success");
			}
			if (rolUsuarioForm.getDescripcionRol() != null) {
				rolUsuarioVO.setDescripcionRol(rolUsuarioForm.getDescripcionRol());
				rolUsuarioVO.setNivelPermiso(rolUsuarioForm.getNivelPermiso());
				rolUsuarioBusiness.altaRolUsuario(rolUsuarioVO);
			} else {
				reset(rolUsuarioForm);
			}	
		} catch(NullPointerException e) {
			return mapping.findForward(MAP_ROL_USUARIO_ERROR);
		}catch(Exception e) {			
			return mapping.findForward(MAP_ROL_USUARIO_ERROR);
		} finally {
			reset(rolUsuarioForm);
		}
		return mapping.findForward(MAP_ROL_USUARIO_SUCCESS);
	}

}
