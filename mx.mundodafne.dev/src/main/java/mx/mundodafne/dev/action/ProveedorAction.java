package mx.mundodafne.dev.action;

import static mx.mundodafne.dev.util.Constantes.MAP_PROVEEDOR_ERROR;
import static mx.mundodafne.dev.util.Constantes.MAP_PROVEEDOR_SUCCESS;
import static mx.mundodafne.dev.util.Constantes.EMPTY_STRING;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import mx.mundodafne.dev.dao.ProveedorDAOImpl;
import mx.mundodafne.dev.form.ProveedorForm;
import mx.mundodafne.dev.vo.ProveedorVO;

public class ProveedorAction extends DispatchAction{

	private ProveedorDAOImpl proveedorDAO = new ProveedorDAOImpl(); 
	
	private void reset(ProveedorVO proveedorVO) {
		proveedorVO.setDireccion(EMPTY_STRING);
		proveedorVO.setRazonSocial(EMPTY_STRING);
		proveedorVO.setRfcProveedor(EMPTY_STRING);
	}
	
	public ActionForward consultaProveedores(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		try {
			ProveedorForm proveedorForm = (ProveedorForm) form;
			ProveedorVO proveedorVO = new ProveedorVO();
			
		} catch(Exception e) {
			return mapping.findForward(MAP_PROVEEDOR_ERROR);
		}
		return mapping.findForward(MAP_PROVEEDOR_SUCCESS);
	}

	public ActionForward altaProveedor(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ProveedorForm proveedorForm = (ProveedorForm) form;
		ProveedorVO proveedorVO = new ProveedorVO();
		try {
			if (proveedorForm != null) {
				proveedorVO.setDireccion(proveedorForm.getDireccion());
				proveedorVO.setFechaAlta(new Date());
				proveedorVO.setRazonSocial(proveedorForm.getRazonSocial());
				proveedorVO.setRfcProveedor(proveedorForm.getRfcProveedor());
				proveedorDAO.altaProveedor(proveedorVO);	
			}				 
		} catch(Exception e) {
			return mapping.findForward(MAP_PROVEEDOR_ERROR);
		} finally {
			reset(proveedorVO);
		}
		return mapping.findForward(MAP_PROVEEDOR_SUCCESS);
	}
	
	
}
