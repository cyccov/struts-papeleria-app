package mx.mundodafne.dev.action;

import static mx.mundodafne.dev.util.Constantes.MAP_USUARIOS_CONSULTA_ERROR;
import static mx.mundodafne.dev.util.Constantes.MAP_USUARIOS_CONSULTA_SUCCESS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.mundodafne.dev.dao.UsuarioDAOImpl;
import mx.mundodafne.dev.form.UsuarioForm;
import mx.mundodafne.dev.vo.UsuarioVO;

public class UsuarioConsultaAction extends Action{
	final static Logger logger = Logger.getLogger(UsuarioConsultaAction.class);
	private UsuarioVO usuarioVO = new UsuarioVO();
	private UsuarioDAOImpl usuarioDAO = new UsuarioDAOImpl();
		
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
//		List listaResultados = new ArrayList();
		try {
			logger.debug("Consultando DB: ");
			UsuarioForm usuarioForm = (UsuarioForm) form;
			usuarioVO.setListaUsuarios(usuarioDAO.obtieneLosUsuarios());
			request.setAttribute("lista_resultados", usuarioVO.getListaUsuarios());
		} catch(Exception e) {
			logger.error("Error en clase UsuarioConsultaAction: ",e);
			return mapping.findForward(MAP_USUARIOS_CONSULTA_ERROR);
		}
		return mapping.findForward(MAP_USUARIOS_CONSULTA_SUCCESS);
	}	
}
