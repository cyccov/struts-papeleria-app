<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="ISO-8859-1">
<title>Catalogo de papeleria</title>
</head>
<body>
	<html:form action="usuarios/catalogo">
	<p>Usuario: <html:text property="nombreUsuario"/></p><br/>
	<p>Contrase;a: <html:text property="passwordUsuario"/></p><br/>
	<p>Rol: </p> <html:select property="rolesUsuario"><html:options name="roles_usuario"/></html:select> <br/>
		<table>
			<tbody>
				<tr>
					<th>Usuario</th>
					<td>test</td>
				</tr>		
				<tr>
					<th>ID</th>
					<td>1</td>
				</tr>
				<tr>
					<th>Rol Asignado</th>
					<td>Administrador</td>
				</tr>
			</tbody>
		</table>
        <html:submit value="">
            <p>Dar de alta</p>
        </html:submit>
	</html:form>	
</body>
</html:html>