<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="ISO-8859-1">
<title>Alta de empleado</title>
<script type="text/javascript">
    
</script>
</head>
<body>
	<html:form action="/empleados?method=cargaCatalogo">
        <p>Nombre: <html:text property="nombreUno" value="nombre_uno"/></p><br/>
        <p>Segundo Nombre: <html:text property="nombreDos"/></p><br/>
        <p>Apellido Paterno: <html:text property="apellidoPaterno"/> </p> <br/>
        <p>Apellido Materno: <html:text property="apellidoMaterno"/> </p> <br/>
        <p>RFC: <html:text property="rfc"/> </p> <br/>
        <p>Fecha de Nacimiento: <html:text property="fechaNacimiento"/> </p> <br/>
        <p>Calle: <html:text property="calle"/> </p> <br/>
        <p>Num. Calle: <html:text property="numeroCalle"/> </p> <br/>
        <p>Ciudad: <html:text property="ciudad"/> </p> <br/>    
        <html:submit>Dar de alta</html:submit>		
        <html:submit property="method" value="altaEmpleado">Nuevo Empleado</html:submit>
        <br><html:link action="/empleados?method=altaEmpleado">Dar de alta empleado</html:link>
	</html:form>	
</body>
</html:html>