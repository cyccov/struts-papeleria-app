<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
	<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
	<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<!DOCTYPE html>
<html:html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Catalogo de usuarios</title>
	</head>
	<body>
		<html:form action="/proveedores?method=altaProveedor">
		<p>RFC Proveedor: <html:text property="rfcProveedor"/></p><br/>
		<p>Razon Social: <html:text property="razonSocial"/></p><br/>
		<p>Direccion: <html:text property="direccion"/></p><br/>
		<html:submit>Dar de alta</html:submit>		
		<html:link action="/welcome">Dar de alta</html:link>
		<br/><html:link action="/proveedores?method=altaProveedor">Nuevo Proveedor</html:link>
		</html:form>
	</body>
</html:html>