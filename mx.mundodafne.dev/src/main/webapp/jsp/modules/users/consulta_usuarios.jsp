<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
	<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
	<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="ISO-8859-1">
<title>Consulta de usuarios</title>
</head>
<body>
	<html:form action="/usuarios/consulta">
    <p>Busqueda de usuarios (todos) </p><br>
		<logic:iterate name="lista_resultados" id="listaResultadosId">
			<li><bean:write name="listaResultadosId" property="nombreUsuario"/></li>
			<li><bean:write name="listaResultadosId" property="usuarioPass"/></li>			
			<li><bean:write name="listaResultadosId" property="rolUsuario"/></li>		
		</logic:iterate> <br><br>
		
			<p>Busqueda de usuario por nombre: </p><br>
			<html:text property="nombreUsuario"/>
			<html:submit>Busqueda por nombre</html:submit>
            <br><br>
			<logic:present scope="session" name="lista_resultados">
				<logic:iterate name="listaUsuarios" id="listaResultadosId">
					<table>
						<tbody>
							<tr>
								<th>ID</th>
								<th>Usuario</th>
								<th>Contraseña</th>
							</tr>									
							<li><tr>
								<td><strong>Nombre:</strong><bean:write name="listaResultadosId" property="nombreUsuario"/></td>
							</tr></li>
							<li><tr>
								<td><strong>Pass:</strong><bean:write name="listaResultadosId" property="usuarioPass"/></td>
							</tr></li>
							<li><tr>
								<td><strong>Rol:</strong><bean:write name="listaResultadosId" property="rolUsuario"/></td>
							</tr></li>
						</tbody>
					</table>
				</logic:iterate>			
			</logic:present>
			<p>Busqueda de usuario por ID: </p><br>
			<html:text property="idUsuario"/>
			<html:submit>Busqueda por ID</html:submit>
            <br><br>
			<%-- <li><bean:write name="resultado_por_id" property="nombreUsuario"/></li>
			<li><bean:write name="resultado_por_id" property="usuarioPass"/></li> --%>

	</html:form>
</body>
</html:html>