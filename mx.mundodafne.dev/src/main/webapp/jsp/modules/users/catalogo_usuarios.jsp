<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
	<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
	<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="ISO-8859-1">
<title>Catalogo de usuarios</title>
</head>
<body>
	<html:form action="/usuarios/catalogo">
	<p>Usuario: <html:text property="nombreUsuario"/></p><br/>
	<p>Contrase;a: <html:text property="passwordUsuario"/></p><br/>
	<p>Rol: </p> 
	<html:select property="rolUsuario">
		<html:options name="rolesUsuario"/>
	</html:select> <br/>
	<!--con bean:write llamamos a la variable rolUsuario del bean (form) usuarioForm-->
	<p>Valor seleccionado: <strong><bean:write name="usuarioForm" property="rolUsuario"/></strong> 
		<table>
			<tbody>
				<tr>
					<th>Usuario</th>
					<td>test</td>
				</tr>		
				<tr>
					<th>ID</th>
					<td>1</td>
				</tr>
				<tr>
					<th>Rol Asignado</th>
					<td>Administrador</td>
				</tr>
			</tbody>
		</table>	
		<strong><p>Usuarios dados de alta: </p></strong>
		<table>
			<tbody>
				<tr>
					<td>Usuario</td>
					<td>Contraseña</td>
					<td>Rol Usuario</td>
				</tr>
					<logic:notPresent scope="session" name="lista_resultados">
						<p>No hay resultados</p>						
					</logic:notPresent>									
				<tr>
				<tr>					
					<logic:present scope="session" name="lista_resultados">
						<logic:iterate name="usuarioForm" property="listaUsuarios" id="idListaUsuarios">
							<li><td><bean:write name="idListaUsuarios" property="nombreUsuario"/></li></td>
							<li><td><bean:write name="idListaUsuarios" property="rolUsuario"/></li></td>
							<li><td><bean:write name="idListaUsuarios" property="usuarioPass"/></li></td>
						</logic:iterate> 					
					</logic:present>					
				</tr>
			</tbody>
		</table>
        <html:submit>Dar de alta</html:submit>		
		<html:link action="/welcome">Dar de alta</html:link>
		<br/><html:link action="/empleados?method=cargaCatalogo">Nuevo Empleado</html:link>		
	</html:form>
</body>
</html:html>