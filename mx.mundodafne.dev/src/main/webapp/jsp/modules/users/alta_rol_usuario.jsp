<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
	<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
	<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="ISO-8859-1">
<title>Catalogo de usuarios</title>
</head>
<body>
	<html:form action="/usuarios/rol">
		<p>Rol: <html:text property="descripcionRol"/></p><br/>
		<%-- <html:errors property="descripcionRol"/> --%>
		<p>Nivel permiso: <html:text property="nivelPermiso"/></p><br/>
    <html:submit>Dar de alta</html:submit>		
	<html:submit value="mandaTransporteEmisor">Mandar mensaje</html:submit>
	</html:form>
</body>
</html:html>